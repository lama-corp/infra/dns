{
  # name = "lama-corp-dns";
  description = "Lama Corp. DNS management";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, futils }:
  let
    inherit (nixpkgs) lib;
    inherit (futils.lib) eachDefaultSystem defaultSystems;
    inherit (lib) recursiveUpdate;

    nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
      inherit system;
    });
  in
  (eachDefaultSystem (system:
    let
      pkgs = nixpkgsFor.${system};
    in
    {
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [
          poetry
        ];
      };
    }));
}
