# Lama Corp. Infrastructure DNS

## Usage

```
poetry install
poetry shell
export CF_TOKEN=<your_very_secret_cloudflare_token>
octodns-sync --config-file=./config/config.yaml
# check that everything looks ok
octodns-sync --config-file=./config/config.yaml --doit
```
